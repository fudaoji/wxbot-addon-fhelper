<?php
/**
 * Created by PhpStorm.
 * Script Name: Setting.php
 * Create: 2023/6/6 15:35
 * Description:
 * Author: fudaoji<fdj@kuryun.cn>
 */

namespace addons\fhelper\admin\controller;


use addons\fhelper\common\library\FmConfig;

class Setting extends Base
{
    function index(){
        if($this->request->isPost()){
            $post_data = input();
            $check = $this->request->checkToken('__token__');
            if (false === $check) {
                fm_set_msg(lng("Invalid Token."), 'danger');
                $this->redirect($_SERVER['HTTP_REFERER']);
            }
            !isset($post_data['show_hidden']) && $post_data['show_hidden'] = false;
            !isset($post_data['hide_Cols']) && $post_data['hide_Cols'] = false;
            !isset($post_data['error_reporting']) && $post_data['error_reporting'] = false;

            unset($post_data['p'], $post_data['__token__']);
            if(($res = FmConfig::save($post_data)) === true){
                fm_set_msg(lng('Saved Successfully'));
            }else{
                fm_set_msg(lng('File Saved Successfully') . $res, 'danger');
            }
            $this->redirect(addon_url('admin/setting/index'));
        }

        $lang_list = fm_lang_list();
        $assign = array_merge($this->configs, [
            'lang_list' => $lang_list,
            'fm_theme' => $this->configs['theme']
        ]);
        return $this->show($assign);
    }
}