<?php
/**
 * Created by PhpStorm.
 * Script Name: info.php
 * Create: 2023/5/24 14:01
 * Description:
 * Author: fudaoji<fdj@kuryun.cn>
 */

$plugin = get_addon_name(__FILE__);
return [
    'type' => 'pc',
    'title' => '文件管理助手',
    'name' => $plugin,
    'desc' => '超实用的文件在线管理器。包含文件（文件夹）的增、删、改、查。',
    'version' => '1.0.0',
    'author' => '苟哥',
    'logo' => 'logo.png',
    'admin_url' => '/addon/'.$plugin.'/admin/index/index',
    'admin_url_type' => 2
];