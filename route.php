<?php

use think\facade\Route;

//应用的路由文件需要返回函数，函数内部实现可完全参考tp6的路由定义规则
//所有的请求：包含get、post等都需要定义路由

$addon = get_addon_name(__FILE__, 2);
$namespace = "\addons\\$addon";

return function () use ($addon, $namespace){
    $admin_namespace = $namespace . "\admin\controller\\";

    Route::group("$addon/admin/uploader", function () use ($admin_namespace) {
        $controller = $admin_namespace . "Uploader::";
        Route::rule('editorpost', $controller . "editorpost");
        Route::rule('filepost', $controller . "filepost");
        Route::rule('picturepost', $controller . "picturepost");
    });

    Route::group("$addon/admin/index", function () use ($admin_namespace) {
        $controller = $admin_namespace . "Index::";
        Route::rule('preview', $controller . "preview");
        Route::rule('chmod', $controller . "chmod");
        Route::rule('unpack', $controller . "unpack");
        Route::rule('backup', $controller . "backup");
        Route::rule('edit', $controller . "edit");
        Route::rule('detail', $controller . "detail");
        Route::rule('masscopy', $controller . "massCopy");
        Route::rule('upload', $controller . "upload");
        Route::rule('download', $controller . "download");
        Route::rule('copy', $controller . "copy");
        Route::rule('rename', $controller . "rename");
        Route::rule('delfile', $controller . "delFile");
        Route::rule('createfile', $controller . "createFile");
        Route::rule('index', $controller . "index");
    });

    Route::group("$addon/admin/setting", function () use ($admin_namespace) {
        $controller = $admin_namespace . "Setting::";
        Route::rule('index', $controller . "index");
    });
};