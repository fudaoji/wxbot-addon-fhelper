<?php
/**
 * Created by PhpStorm.
 * Script Name: Common.php
 * Create: 2023/5/25 8:20
 * Description:
 * Author: fudaoji<fdj@kuryun.cn>
 */

namespace addons\fhelper\common\model;


use app\common\model\Base;

class Common extends Base
{
    //protected $isCache = true;

    public function __construct($data = [])
    {
        $this->table = $this->getTablePrefix() . 'fhelper_'.$this->table;
        parent::__construct($data);
    }
}