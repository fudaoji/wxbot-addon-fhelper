<?php
/**
 * Created by PhpStorm.
 * Script Name: menu.php
 * Create: 12/17/22 10:24 AM
 * Description: 功能菜单
 * Author: fudaoji<fdj@kuryun.cn>
 */

return [
    [
        'title' => '后台管理',
        'children' => [
            ['title' => '首页', 'node' => 'admin/index/welcome']
        ]
    ]
];