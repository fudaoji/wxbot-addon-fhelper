# 文件管理器(fhelper)

#### 介绍
文件管理器(fhelper)是参考[TinyFileManager](https://tinyfilemanager.github.io/)并用thinkphp6实现的一个在线文件管理器工具。
[TinyFileManager](https://tinyfilemanager.github.io/)是基于Web的PHP文件管理器，是一个简单、快速、体积小的单一PHP文件，可以放置在服务器上的任何文件夹中，支持多语言，用于通过Web浏览器在线存储、上传、编辑和管理文件和文件夹的Web应用程序。该应用程序在PHP 5.5+上运行，允许创建多个用户，每个用户可以拥有自己的目录，并内置支持使用cloud9 IDE管理文本文件，支持超过150多种语言的语法高亮和35多种主题。

#### 软件架构
fhelper不支持独立部署，需要先安装[wxbot](https://gitee.com/fudaoji/wxbot)。然后在应用市场免费下载安装。

![功能地图](fhelper.png)

#### 安装教程

1.  安装[wxbot](https://daoadmin.kuryun.com/home/guide/bot/id/75/v/1.x.html)
2.  在wxbot的应用市场中下载
3.  在wxbot的应用管理中安装fhelper

#### 感谢

- [Wxbot](https://gitee.com/fudaoji/wxbot)
- [TinyFileManager](https://tinyfilemanager.github.io/)